//
//  ViewControllerRootView.swift
//  TP
//
//  Created by Гезаль Амир on 19.03.2024.
//

import UIKit

final class ViewControllerRootView: UIView {
    
    private let tableHandler = TableViewHandler()
    
    private lazy var tableView: UITableView = {
        $0.separatorStyle = .none
        $0.delegate = tableHandler
        $0.dataSource = tableHandler
        $0.showsVerticalScrollIndicator = false
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.register(TableCell.self, forCellReuseIdentifier: TableCell.id)
        return $0
    }(UITableView())
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private func setupSubviews() {
        addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.centerXAnchor.constraint(equalTo: centerXAnchor),
            tableView.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
}

extension ViewControllerRootView: ItemsProvider {
    var randomUpdatableItems: [RandomItemUpdateble] {
        tableView.visibleCells.compactMap { $0 as? RandomItemUpdateble }
    }
}
