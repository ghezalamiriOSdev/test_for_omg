//
//  TableViewHandler.swift
//  TP
//
//  Created by Гезаль Амир on 19.03.2024.
//

import UIKit

final class TableViewHandler: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private let maximumCount = 1000
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.dequeueReusableCell(withIdentifier: TableCell.id, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int.random(in: 10..<maximumCount)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        TableCell.Config.itemSize + 10
    }
}
