//
//  ViewController.swift
//  TP
//
//  Created by Гезаль Амир on 19.03.2024.
//

import UIKit

final class ViewController: UIViewController {

    private var viewModel: ViewModel!
    
    override func loadView() {
        super.loadView()
        let rootView = ViewControllerRootView()
        view = rootView
        viewModel = ViewModel(itemsProvider: rootView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.onViewDidLoad()
    }
}

