//
//  CellAnimator.swift
//  TP
//
//  Created by Гезаль Амир on 21.03.2024.
//

import UIKit


final class AnimationHandler {
    
    private var isAnimatingDownscale = false
    private var isAnimatingUpscale = false
    
    private var downscaleAnimator: UIViewPropertyAnimator?
    private var upscaleAnimator: UIViewPropertyAnimator?
    
    unowned private let viewToTransform: UIView
    
    init(viewToTransform: UIView) {
        self.viewToTransform = viewToTransform
    }
    
    func handleHighlighted(_ isHighlighted: Bool) {
        if isHighlighted {
            stopAnimating(.upscale)
            startAnimation(.downscale)
        } else {
            stopAnimating(.downscale)
            startAnimation(.upscale)
        }
    }
}

private extension AnimationHandler {
    enum Animation {
        case downscale
        case upscale
    }
    
    enum Config {
        static let scale: CGFloat = 0.8
        static let duration: TimeInterval = 0.5
    }
    
    func createDownscaleAnimator() -> UIViewPropertyAnimator {
        let currentTransform = viewToTransform.transform.a
        let progress = 1 - (1 - currentTransform)
        let duration = Config.duration * progress
        
        let animator = UIViewPropertyAnimator(duration: duration, curve: .linear) { [weak self] in
            self?.viewToTransform.transform = .init(scaleX: Config.scale, y: Config.scale)
        }
        
        animator.addCompletion { [weak self] _ in
            self?.downscaleAnimator = nil
        }
        
        downscaleAnimator = animator
        
        return animator
    }
    
    func createUpscaleAnimator() -> UIViewPropertyAnimator {
        let currentTransform = viewToTransform.transform.a
        let progress = 1 - (currentTransform - Config.scale)/(1 - Config.scale)
        let duration = Config.duration * progress
        
        let animator = UIViewPropertyAnimator(duration: duration, curve: .linear) { [weak self] in
            self?.viewToTransform.transform = .identity
        }
        
        animator.addCompletion { [weak self] _ in
            self?.upscaleAnimator = nil
        }
        
        upscaleAnimator = animator
        
        return animator
    }
    
    func stopAnimating(_ animation: Animation) {
        switch animation {
        case .downscale:
            downscaleAnimator?.stopAnimation(true)
            downscaleAnimator = nil
            isAnimatingDownscale = false
        case .upscale:
            upscaleAnimator?.stopAnimation(true)
            upscaleAnimator = nil
            isAnimatingUpscale = false
        }
    }
    
    func startAnimation(_ animation: Animation) {
        switch animation {
        case .downscale:
            guard !isAnimatingDownscale else { return }
            isAnimatingDownscale = true
            createDownscaleAnimator().startAnimation()
        case .upscale:
            guard !isAnimatingUpscale else { return }
            isAnimatingUpscale = true
            createUpscaleAnimator().startAnimation()
        }
    }
}
