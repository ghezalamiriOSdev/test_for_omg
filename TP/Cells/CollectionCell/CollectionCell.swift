//
//  CollectionCell.swift
//  TP
//
//  Created by Гезаль Амир on 19.03.2024.
//

import UIKit

final class CollectionCell: UICollectionViewCell {
    
    static let id = "SquareView"
    
    private var shapeLayer: CAShapeLayer!
    private var cellAnimator: AnimationHandler?
    
    override var isHighlighted: Bool {
        didSet { handleHighlighted() }
    }
    
    private lazy var titleLabel: UILabel = {
        $0.textColor = .white
        $0.font = .systemFont(ofSize: 20, weight: .bold)
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.text = "1"
        return $0
    }(UILabel())
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawShape(in: rect)
    }
    
    func updateTitle(with number: Int) {
        titleLabel.text = "\(number)"
        onUpdatedAnimation()
    }
}

private extension CollectionCell {
    enum Config {
        static let duration = 0.2
        static let strokeWidth: CGFloat = 3
        static let fillColor = UIColor.red.cgColor
        static let strokeColor = UIColor.green.cgColor
        static let updateColor = UIColor.green.cgColor
    }
}

private extension CollectionCell {
    func setupSubviews() {
        addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    func drawShape(in rect: CGRect) {
        if shapeLayer == nil {
            shapeLayer = CAShapeLayer()
            shapeLayer.fillColor = Config.fillColor
            shapeLayer.lineWidth = Config.strokeWidth
            shapeLayer.strokeColor = Config.strokeColor
            
            let radius = CGFloat.random(in: 0..<frame.width/2)
            shapeLayer.path = UIBezierPath(
                roundedRect: CGRect(x: .zero, y: .zero, width: rect.width, height: rect.height),
                cornerRadius: radius
            ).cgPath
            
            layer.insertSublayer(shapeLayer, at: .zero)
        }
    }
    
    func onUpdatedAnimation() {
        let animation = CABasicAnimation(keyPath: "fillColor")
        animation.duration = Config.duration
        animation.toValue = Config.updateColor
        shapeLayer.add(animation, forKey: "animation")
    }
    
    func handleHighlighted() {
        if isHighlighted, cellAnimator == nil {
            cellAnimator = AnimationHandler(viewToTransform: self)
        }
        guard cellAnimator != nil else { return }
        cellAnimator?.handleHighlighted(isHighlighted)
    }
}
