//
//  TableCell.swift
//  TP
//
//  Created by Гезаль Амир on 19.03.2024.
//

import UIKit

protocol RandomItemUpdateble {
    func updateItem(at: Int, with: Int)
}

final class TableCell: UITableViewCell {
    static let id = "TableCell"
    
    enum Config {
        static let itemSize: CGFloat = 40
        fileprivate static let maximumCount = 1000
        fileprivate static let count = Int.random(in: 10..<Config.maximumCount)
        fileprivate static let spacing: CGFloat = 8
    }
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(CollectionCell.self, forCellWithReuseIdentifier: CollectionCell.id)
        return collectionView
    }()
    
    private lazy var flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = Config.spacing
        layout.itemSize = .init(width: Config.itemSize, height: Config.itemSize)
        return layout
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private func setupSubviews() {
        contentView.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.heightAnchor.constraint(equalToConstant: Config.itemSize),
            collectionView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            collectionView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])
    }
}

extension TableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Config.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCell.id, for: indexPath)
    }
}

extension TableCell: RandomItemUpdateble {
    func updateItem(at index: Int, with value: Int) {
        (collectionView.visibleCells[safe: index] as? CollectionCell)?.updateTitle(with: value)
    }
}
