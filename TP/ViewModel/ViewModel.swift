//
//  ViewModel.swift
//  TP
//
//  Created by Гезаль Амир on 19.03.2024.
//

import Foundation

protocol ItemsProvider: AnyObject {
    var randomUpdatableItems: [RandomItemUpdateble] { get }
}

final class ViewModel {
    
    private var timer: Timer?
    private unowned let itemsProvider: ItemsProvider
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    init(itemsProvider: ItemsProvider) {
        self.itemsProvider = itemsProvider
    }
    
    func onViewDidLoad() {
        fireTimer()
    }
}

private extension ViewModel {
    func fireTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] _ in
            self?.updateItems()
            // ТК насчет стейта можно было не заморачиваться, то не стал обрабатывать сохранение стейтов ячеек
        })
        
        RunLoop.main.add(timer!, forMode: .tracking)
    }
    
    func updateItems() {
        itemsProvider.randomUpdatableItems.forEach {
            let index = Int.random(in: 0..<10)
            let value = Int.random(in: 0..<10)
            $0.updateItem(at: index, with: value)
        }
    }
}
